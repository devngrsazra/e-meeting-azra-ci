<p><center><h1>Form Reservation</h1></center></p>
<form action="<?= site_url('Front/reserv') ?>" method="POST"> 
    <p>
    <table width="25%">	
        <tr>
            <td>
                Nama Ruangan
            </td>
            <td>
                <select name="id_ruangan">
                    <option>-- Pilih Ruangan --</option>
                    <?php foreach ($ruangan as $r) { ?>
                        <option value="<?= $r->id_ruangan ?>"><?= $r->nama_ruangan ?></option>
                    <?php } ?>
                </select>
            </td>
        <tr>
        <tr>
            <td>
                Tanggal
            </td>
            <td>
                <input name="tanggal_acara" type="date" placeholder="tgl">
            </td>
        <tr>
        <tr>
            <td>
                Jam
            </td>
            <td>
                <input name="jam" type="time" placeholder="jam">
            </td>
        <tr>
        <tr>
            <td>
                Judul Acara
            </td>
            <td>
                <input name="judul_acara" type="text" placeholder="judul acara">
            </td>
        <tr>
        <tr>
            <td>
                Pengundang Rapat
            </td>
            <td>
                <input name="pengundang_rapat" type="text" placeholder="pengundang rapat">
            </td>
        <tr>
        <tr>
            <td>
                Email Pengundang Rapat
            </td>
            <td>
                <input name="email" type="email" placeholder="email pengundang rapat">
            </td>
        <tr>
        <tr>
            <td>
                Telpon
            </td>
            <td>
                <input name="telepon" type="text" placeholder="telpon">
            </td>
        <tr>
        <tr>
            <td>
                <input type="submit" value="input" name="reserv">
            </td>
        <tr>
    </table>
</p>
</form>