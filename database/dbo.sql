/*
 Navicat Premium Data Transfer

 Source Server         : SQL SERVER
 Source Server Type    : SQL Server
 Source Server Version : 10504000
 Source Host           : DESKTOP-18M2IUL:1433
 Source Catalog        : azra
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 10504000
 File Encoding         : 65001

 Date: 07/05/2019 14:47:22
*/


-- ----------------------------
-- Table structure for tbl_booking
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_booking]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_booking]
GO

CREATE TABLE [dbo].[tbl_booking] (
  [id_booking] int  NOT NULL,
  [id_ruangan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tanggal_acara] date  NULL,
  [jam] varchar(10) COLLATE Latin1_General_CI_AS  NULL,
  [judul_acara] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [pengundang_rapat] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [email] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [telepon] int  NOT NULL
)
GO

ALTER TABLE [dbo].[tbl_booking] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_booking
-- ----------------------------
INSERT INTO [dbo].[tbl_booking] VALUES (N'1', N'1', N'2019-05-07', N'13:00', N'aba', N'ada', N'uknow@gmail.com', N'8112555')
GO

INSERT INTO [dbo].[tbl_booking] VALUES (N'2', N'1', N'2019-05-08', N'12:00', N'ad', N'ada', N'i@i.com', N'81231')
GO


-- ----------------------------
-- Table structure for tbl_ruangan
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[tbl_ruangan]') AND type IN ('U'))
	DROP TABLE [dbo].[tbl_ruangan]
GO

CREATE TABLE [dbo].[tbl_ruangan] (
  [id_ruangan] int  NOT NULL,
  [nama_ruangan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [kapasitas] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[tbl_ruangan] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of tbl_ruangan
-- ----------------------------
INSERT INTO [dbo].[tbl_ruangan] VALUES (N'1', N'Meeting 1', N'50')
GO

INSERT INTO [dbo].[tbl_ruangan] VALUES (N'2', N'Meeting 2', N'55')
GO


-- ----------------------------
-- Primary Key structure for table tbl_booking
-- ----------------------------
ALTER TABLE [dbo].[tbl_booking] ADD CONSTRAINT [PK__tbl_book__09A635DD07020F21] PRIMARY KEY CLUSTERED ([id_booking])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table tbl_ruangan
-- ----------------------------
ALTER TABLE [dbo].[tbl_ruangan] ADD CONSTRAINT [PK__tbl_ruan__E882AADC0AD2A005] PRIMARY KEY CLUSTERED ([id_ruangan])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

